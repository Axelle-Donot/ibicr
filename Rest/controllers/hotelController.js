// controllers/hotelController.js
import grpc from '@grpc/grpc-js';
import protoLoader from '@grpc/proto-loader';

const PROTO_PATH = './../Grpc/hotel.proto';

const packageDefinition = protoLoader.loadSync(PROTO_PATH, {
  keepCase: true,
  longs: String,
  enums: String,
  defaults: true,
  oneofs: true,
});

const hotelProto = grpc.loadPackageDefinition(packageDefinition).hotel;
const client = new hotelProto.HotelService('localhost:50051', grpc.credentials.createInsecure());

export const getRoomByType = (req, res) => {
  const roomType = req.params.roomType;

  client.GetRoomInfo({ room_type: roomType }, (error, response) => {
    if (error) {
      return res.status(500).json({ message: error.details });
    }
    res.status(200).json(response);
  });
};

export async function getRoomById(roomId) {
  return new Promise((resolve, reject) => {
    client.getRoomById({ room_id: roomId }, (error, response) => {
      if (error) {
        reject(error.details);
      } else {
        resolve(response);
      }
    });
  });
}

export const getRoomByDate = (req, res) => {
  const roomDateD = req.body.roomDateD;
  const roomDateF = req.body.roomDateF;
  console.log("dates :  ", roomDateD, roomDateF)

  client.GetRoomByDate({ dateD: roomDateD, dateF : roomDateF }, (error, response) => {
    if (error) {
      return res.status(500).json({ message: error.details });
    }
    res.status(200).json(response);
  });
};

export const getRooms = (req, res) => {
  // Si l'API gRPC ne supporte pas la récupération de toutes les chambres,
  // vous devez implémenter une logique alternative ici.
  // Pour cet exemple, nous supposons que nous avons une fonction 'GetAllRooms' dans l'API gRPC
  client.GetAllRooms({}, (error, response) => {
    if (error) {
      return res.status(500).json({ message: error.details });
    }
    res.status(200).json(response.rooms);
  });
};
