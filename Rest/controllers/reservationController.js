import {getAvionById} from './avionController.js';
import {getRoomById} from './hotelController.js';

export const makeBook = async (req, res) => {
  const { startDate, endDate, avionId, roomId } = req.body;
  try {
    const avion = getAvionById(avionId);
    const  hotel = await getRoomById(roomId);
    console.log(hotel, "hotel")
    const reservationDetails = {
      startDate,
      endDate,
      avion,
      hotel
    };

    return res.status(200).json(reservationDetails);
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export default { makeBook };
