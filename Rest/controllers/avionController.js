
const avions = [
  { id: 1, model: 'Boeing 737', airline: 'Air France', place: '10a', date: '2024-07-01T00:00:00Z' },
  { id: 2, model: 'Boeing 737', airline: 'Air France', place: '11a', date: '2024-07-02T00:00:00Z' },
  { id: 3, model: 'Boeing 737', airline: 'Air France', place: '12a', date: '2024-07-03T00:00:00Z' },
  { id: 4, model: 'Boeing 737', airline: 'Air France', place: '13a', date: '2024-07-04T00:00:00Z' },
  { id: 5, model: 'Boeing 737', airline: 'Air France', place: '14a', date: '2024-07-05T00:00:00Z' },
  { id: 6, model: 'Airbus A320', airline: 'Lufthansa', place: '1a', date: '2024-07-06T00:00:00Z' },
  { id: 7, model: 'Airbus A320', airline: 'Lufthansa', place: '4a', date: '2024-07-07T00:00:00Z' },
  { id: 9, model: 'Airbus A320', airline: 'Lufthansa', place: '134a', date: '2024-07-08T00:00:00Z' },
  { id: 10, model: 'Airbus A320', airline: 'Lufthansa', place: '1334a', date: '2024-07-09T00:00:00Z' },
  { id: 11, model: 'Airbus A320', airline: 'Lufthansa', place: '1004a', date: '2024-07-09T00:00:00Z' },
  { id: 12, model: 'Airbus A320', airline: 'Lufthansa', place: '124a', date: '2024-07-11T00:00:00Z' },
  { id: 13, model: 'Boeing 777', airline: 'Emirates', place: '1', date: '2024-07-12T00:00:00Z' },
  { id: 14, model: 'Boeing 777', airline: 'Emirates', place: '1678', date: '2024-07-13T00:00:00Z' },
  { id: 15, model: 'Boeing 777', airline: 'Emirates', place: '1tdfdfo', date: '2024-07-14T00:00:00Z' },
  { id: 16, model: 'Boeing 777', airline: 'Emirates', place: '12345', date: '2024-07-15T00:00:00Z' },
  { id: 17, model: 'Boeing 777', airline: 'Emirates', place: '1098765', date: '2024-07-16T00:00:00Z' },
  { id: 18, model: 'Boeing 787', airline: 'Qatar Airways', place: '10b', date: '2024-07-15T00:00:00Z' },
  { id: 19, model: 'Boeing 787', airline: 'Qatar Airways', place: '11b', date: '2024-07-18T00:00:00Z' },
  { id: 20, model: 'Boeing 787', airline: 'Qatar Airways', place: '12b', date: '2024-07-19T00:00:00Z' },
  { id: 21, model: 'Boeing 747', airline: 'British Airways', place: '1c', date: '2024-07-03T00:00:00Z' },
  { id: 22, model: 'Boeing 747', airline: 'British Airways', place: '2c', date: '2024-07-21T00:00:00Z' },
  { id: 23, model: 'Boeing 747', airline: 'British Airways', place: '3c', date: '2024-07-22T00:00:00Z' },
  { id: 24, model: 'Boeing 747', airline: 'British Airways', place: '4c', date: '2024-07-23T00:00:00Z' },
  { id: 25, model: 'Airbus A380', airline: 'Singapore Airlines', place: '5d', date: '2024-07-24T00:00:00Z' },
  { id: 26, model: 'Airbus A380', airline: 'Singapore Airlines', place: '6d', date: '2024-07-14T00:00:00Z' },
  { id: 27, model: 'Airbus A380', airline: 'Singapore Airlines', place: '7d', date: '2024-07-15T00:00:00Z' },
  { id: 28, model: 'Airbus A380', airline: 'Singapore Airlines', place: '8d', date: '2024-07-16T00:00:00Z' }
];

// Fonction pour obtenir un avion par ID
export function getAvionById(avionId) {
  const avion = avions.find(a => a.id === avionId);
  if (!avion) {
    throw new Error('Avion non trouvé');
  }
  return avion;
}

export const getAvions = (req, res) => {
  res.status(200).json(avions);
};


export const getAvionByDate = (req, res) => {
  const date = req.body.dateD ;
  const avionsByDate = avions.filter(a => new Date(a.date).toISOString().split('T')[0] === date);
  if (avionsByDate.length === 0) {
    throw new Error('Aucun avion trouvé pour cette date');
  }
  res.status(200).json(avionsByDate);
};

