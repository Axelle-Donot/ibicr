import express from 'express';
import session from 'express-session';
import passport from 'passport';
import cors from 'cors';
import fetch from 'node-fetch';
import { Strategy as GoogleStrategy } from 'passport-google-oauth20';

import hotel from './routes/hotel.js';
import avion from './routes/avion.js';
import reservation from './routes/reservation.js';


const app = express();
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
  res.setHeader('Access-Control-Allow-Credentials', 'true');
  next();
});

app.use(cors({
  origin: 'https://9000-idx-front-1719300718508.cluster-blu4edcrfnajktuztkjzgyxzek.cloudworkstations.dev',
  credentials: true
}));



// Configuration de express-session (auth Google)
app.use(session({
  secret: 'blabla_trucnul',
  resave: false,
  saveUninitialized: false
}));


app.use(passport.initialize());
app.use(passport.session());


const users = []; 

// Sérialisation de l'utilisateur
passport.serializeUser((user, done) => {
  done(null, user.googleId);
});

// Désérialisation de l'utilisateur
passport.deserializeUser((id, done) => {
  const user = users.find(user => user.googleId === id);
  done(null, user);
});

passport.use(new GoogleStrategy({
  clientID: '88887270287-4flrqv8kmr5qd7pa53r36sp21vev1f3r.apps.googleusercontent.com',
  clientSecret: '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',
  callbackURL: 'https://3000-idx-back-1719237380618.cluster-rcyheetymngt4qx5fpswua3ry4.cloudworkstations.dev/auth/google/callback',
  scope: ['profile', 'email']
},
(accessToken, refreshToken, profile, done) => {
  let user = users.find(u => u.googleId === profile.id);

  if (user) {
    return done(null, user);
  }

  user = {
    googleId: profile.id,
    name: profile.displayName,
    email: profile.emails ? profile.emails[0].value : '',
    accessToken,
  };
  users.push(user);
  done(null, user);
}));

app.use('/room', hotel);
app.use('/avion', avion);
app.use('/reservation', reservation);

app.get('/auth/google', passport.authenticate('google'));

/*app.get('/auth/google', passport.authenticate('google', {
  successRedirect: '/avion/all',
  failureRedirect: '/room/all'
}));*/


/*app.get('/auth/google/callback',
  passport.authenticate('google', {
    successRedirect: 'https://9000-idx-front-1719300718508.cluster-blu4edcrfnajktuztkjzgyxzek.cloudworkstations.dev/profile',
    failureRedirect: 'https://9000-idx-front-1719300718508.cluster-blu4edcrfnajktuztkjzgyxzek.cloudworkstations.dev/'
  })
);*/

app.get('/auth/google/callback',
  passport.authenticate('google', { failureRedirect: '/' }),
  async (req, res) => {
    // Utiliser le token d'accès pour obtenir le profil de l'utilisateur
    try {
      const { accessToken } = req.user;
      const response = await fetch('https://www.googleapis.com/oauth2/v1/userinfo?alt=json', {
        headers: {
          Authorization: `Bearer ${accessToken}`
        }
      });
      const profile = await response.json();
      console.log(profile);
      // Rediriger vers la page de profil avec les données du profil utilisateur
      res.redirect(`https://9000-idx-front-1719300718508.cluster-blu4edcrfnajktuztkjzgyxzek.cloudworkstations.dev/profile?name=${profile.name}&email=${profile.email}&picture=${profile.picture}`);
    } catch (error) {
      console.error('Failed to fetch user profile:', error);
      res.redirect('https://9000-idx-front-1719300718508.cluster-blu4edcrfnajktuztkjzgyxzek.cloudworkstations.dev/');
    }
  }
);

app.get('/', (req, res) => {
  console.log(req)
  const name = process.env.NAME || 'World';
  res.send(`Hello ${name}!`);
});

const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`Serveur en écoute sur le port ${port}`);
});

