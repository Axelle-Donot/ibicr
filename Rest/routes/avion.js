import express from 'express';
import cors from 'cors';
import { getAvions, getAvionByDate } from './../controllers/avionController.js';


const router = express.Router();

router.use(express.json());
const disableCSP = (req, res, next) => {
    res.setHeader('Content-Security-Policy', "script-src 'self'");
    next();
  };
  
  // Utilisation du middleware
  router.use(disableCSP);

// Route sécurisée
router.get('/all', getAvions);
router.post('/date', getAvionByDate);

export default router;
