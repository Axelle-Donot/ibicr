import express from 'express';
import { getRoomByType, getRooms, getRoomByDate} from './../controllers/hotelController.js';


const router = express.Router();
router.use(express.json());

router.get('/type/:type', getRoomByType);
router.get('/all', getRooms);
router.post('/date', getRoomByDate);

export default router;
