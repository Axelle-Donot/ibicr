import express from 'express';
import { makeBook } from './../controllers/reservationController.js';

const router = express.Router();
router.use(express.json());

router.post('/book', makeBook);

export default router;
 