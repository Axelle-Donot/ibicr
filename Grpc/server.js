import grpc from '@grpc/grpc-js';
import protoLoader from '@grpc/proto-loader';

const PROTO_PATH = './hotel.proto';

const packageDefinition = protoLoader.loadSync(PROTO_PATH, {
  keepCase: true,
  longs: String,
  enums: String,
  defaults: true,
  oneofs: true,
});

const hotelProto = grpc.loadPackageDefinition(packageDefinition).hotel;

// Données statiques pour les chambres d'hôtel
const rooms = [
  {
    id: 1,
    room_type: 'basic',
    price: 55,
    dates: [
      '2024-07-01T00:00:00Z', '2024-07-02T00:00:00Z', '2024-07-03T00:00:00Z', '2024-07-04T00:00:00Z', '2024-07-05T00:00:00Z', 
      '2024-07-06T00:00:00Z', '2024-07-07T00:00:00Z'
    ]
  },
  {
    id: 2,
    room_type: 'middle',
    price: 110,
    dates: [
      '2024-07-08T00:00:00Z', '2024-07-09T00:00:00Z', '2024-07-10T00:00:00Z', '2024-07-11T00:00:00Z', '2024-07-12T00:00:00Z', 
      '2024-07-13T00:00:00Z', '2024-07-14T00:00:00Z'
    ]
  },
  {
    id: 3,
    room_type: 'high',
    price: 160,
    dates: [
      '2024-07-15T00:00:00Z', '2024-07-16T00:00:00Z', '2024-07-17T00:00:00Z', '2024-07-18T00:00:00Z', '2024-07-19T00:00:00Z', 
      '2024-07-20T00:00:00Z', '2024-07-21T00:00:00Z'
    ]
  },
  {
    id: 4,
    room_type: 'deluxe',
    price: 210,
    dates: [
      '2024-07-22T00:00:00Z', '2024-07-23T00:00:00Z', '2024-07-24T00:00:00Z', '2024-07-25T00:00:00Z', '2024-07-26T00:00:00Z', 
      '2024-07-27T00:00:00Z', '2024-07-28T00:00:00Z'
    ]
  },
  {
    id: 5,
    room_type: 'luxury',
    price: 260,
    dates: [
      '2024-07-29T00:00:00Z', '2024-07-30T00:00:00Z', '2024-07-31T00:00:00Z'
    ]
  },
  {
    id: 6,
    room_type: 'premium',
    price: 300,
    dates: [
      '2024-07-01T00:00:00Z', '2024-07-02T00:00:00Z', '2024-07-03T00:00:00Z', '2024-07-04T00:00:00Z', '2024-07-05T00:00:00Z', 
      '2024-07-06T00:00:00Z', '2024-07-07T00:00:00Z'
    ]
  },
  {
    id: 7,
    room_type: 'suite',
    price: 350,
    dates: [
      '2024-07-08T00:00:00Z', '2024-07-09T00:00:00Z', '2024-07-10T00:00:00Z', '2024-07-11T00:00:00Z', '2024-07-12T00:00:00Z', 
      '2024-07-13T00:00:00Z', '2024-07-14T00:00:00Z'
    ]
  },
  {
    id: 8,
    room_type: 'executive',
    price: 400,
    dates: [
      '2024-07-15T00:00:00Z', '2024-07-16T00:00:00Z', '2024-07-17T00:00:00Z', '2024-07-18T00:00:00Z', '2024-07-19T00:00:00Z', 
      '2024-07-20T00:00:00Z', '2024-07-21T00:00:00Z'
    ]
  },
  {
    id: 9,
    room_type: 'presidential',
    price: 500,
    dates: [
      '2024-07-22T00:00:00Z', '2024-07-23T00:00:00Z', '2024-07-24T00:00:00Z', '2024-07-25T00:00:00Z', '2024-07-26T00:00:00Z', 
      '2024-07-27T00:00:00Z', '2024-07-28T00:00:00Z'
    ]
  }
];



function getAllRooms(call, callback) {
  const allRooms = Object.values(rooms);  // Convertir l'objet rooms en un tableau
  callback(null, { rooms: allRooms });
}

function getRoomInfo(call, callback) {
  const roomType = call.request.room_type;
  console.log("Requested room_type:", roomType);
  
  const room = Object.values(rooms).find(r => r.room_type === roomType);
  if (room) {
    callback(null, room);
  } else {
    callback({
      code: grpc.status.NOT_FOUND,
      details: "Room not found"
    });
  }
}



function bookRoom(call, callback) {
  const { room_type, check_in_date, check_out_date } = call.request;
  if (rooms[room_type]) {
    callback(null, { confirmation: `Booking confirmed from ${check_in_date} to ${check_out_date}` });
  } else {
    callback({
      code: grpc.status.NOT_FOUND,
      details: "Room not found"
    });
  }
}

function getRoomById(call, callback) {
  const roomId = call.request.room_id;
  console.log("Requested roomId:", call.request);
  
  const room = Object.values(rooms).find(r => r.id == roomId);
  console.log(room)
  if (room) {
    callback(null, room);
  } else {
    callback({
      code: grpc.status.NOT_FOUND,
      details: "Room not found"
    });
  }
}

function getRoomByDate(call, callback) {
  const dateD = call.request.dateD;
  const dateF = call.request.dateF;
  console.log("Requested date:", dateD, ' à ', dateF);

  const roomsByDate = rooms.filter(room => room.dates.includes(dateD));
  const roomsFinal = roomsByDate.filter(room => room.dates.includes(dateF))
  console.log(roomsFinal.length)
    
  if (roomsFinal.length > 0) {
    callback(null, { rooms: roomsFinal });
  } else {
    callback({
      code: grpc.status.NOT_FOUND,
      details: "No rooms available for the given date"
    });
  }
}

function main() {
  const server = new grpc.Server();
  server.addService(hotelProto.HotelService.service, {
    GetRoomInfo: getRoomInfo,
    BookRoom: bookRoom,
    GetAllRooms: getAllRooms,
    GetRoomById: getRoomById,
    GetRoomByDate: getRoomByDate,
  });
  server.bindAsync('0.0.0.0:50051', grpc.ServerCredentials.createInsecure(), () => {
    console.log('Server running at http://127.0.0.1:50051');
    server.start();
  });
}

main();
