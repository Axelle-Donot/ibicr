import grpc from '@grpc/grpc-js';
import protoLoader from '@grpc/proto-loader';

const PROTO_PATH = './hotel.proto';

const packageDefinition = protoLoader.loadSync(PROTO_PATH, {
  keepCase: true,
  longs: String,
  enums: String,
  defaults: true,
  oneofs: true,
});

const hotelProto = grpc.loadPackageDefinition(packageDefinition).hotel;

function main() {
  const client = new hotelProto.HotelService('localhost:50051', grpc.credentials.createInsecure());

  client.GetRoomInfo({ room_type: 'middle' }, (err, response) => {
    if (err) {
      console.error(err);
    } else {
      console.log('Room Info:', response);
    }
  });

  client.BookRoom({ room_type: 'middle', check_in_date: '2024-07-01', check_out_date: '2024-07-10' }, (err, response) => {
    if (err) {
      console.error(err);
    } else {
      console.log('Booking Response:', response);
    }
  });
}

main();
